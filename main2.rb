class Cache
	attr_reader :size, :cola

	def  initialize(size)
		@size = size
		@cola = []
	end

	def put_lru(pair)
		@cola << pair
		@cola.delete_at(0) if @cola.size > @size
	end

	def get_lru(k)
		val = @cola.select {|item| item.has_key?(k)}
		if val.size > 0
			val_recent = val[0]
			@cola.delete(val[0])
			@cola << val_recent
			@cola.delete_at(0) if @cola.size > @size
			return k
		else
			return -1
		end
	end
end

# cache.put_lru(1, 1);		-> [{1:1}]
# cache.put_lru(2, 2);		-> [{1:1},{2:2}]
# cache.get_lru(1);       // returns 1 -> [{2:2},{1:1}]
# cache.put_lru(3, 3);    // evicts key 2 -> [{1:1},{3:3}]
# cache.get_lru(2);       // returns -1 (not found) ->  [{1:1},{3:3}]
# cache.put_lru(4, 4);    // evicts key 1 -> [{3:3},{4:4}]
# cache.get_lru(1);       // returns -1 (not found)
# cache.get_lru(3);       // returns 3 -> [{4:4},{3:3}]
# cache.get_lru(4);       // returns 4 -> [{3:3},{4:4}]


cache = Cache.new(2)

puts "return: #{cache.put_lru({1=>1})} -> #{cache.cola}"
puts "return: #{cache.put_lru({2=>2})} -> #{cache.cola}"
puts "return: #{cache.get_lru(1)} -> #{cache.cola}"
puts "return: #{cache.put_lru({3=>3})} -> #{cache.cola}"
puts "return: #{cache.get_lru(2)} -> #{cache.cola}"   
puts "return: #{cache.put_lru({4=>4})} -> #{cache.cola}" 
puts "return: #{cache.get_lru(1)} -> #{cache.cola}"    
puts "return: #{cache.get_lru(3)} -> #{cache.cola}"    
puts "return: #{cache.get_lru(4)} -> #{cache.cola}"    