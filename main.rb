
def mayor(v)
	maxv = v[0]
	pos_max = 0
	(1..v.size-1).each do |i|
		if(v[i] > maxv)
			maxv = v[i]
			pos_max = i
		end
	end
	sec_max = -1
	pos_sec_max = 0
	(0..v.size-1).each do |i|
		if (v[i] >= sec_max and i != pos_max)
			sec_max = v[i]
			pos_sec_max = i
		end
	end
	pos_sec_max
end

a = [ 3,3,1,2,5 ]
b = [ 5,3,1,3,2 ]
posa = mayor(a)
posb = mayor(b)
puts "El numero mayor para el vector a es #{a[posa]}"
puts "El numero mayor para el vector b es #{b[posb]}"